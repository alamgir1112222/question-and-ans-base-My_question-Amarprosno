-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 25, 2017 at 02:19 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `amarproshno`
--

-- --------------------------------------------------------

--
-- Table structure for table `answers`
--

CREATE TABLE `answers` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `description` varchar(150) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `answers`
--

INSERT INTO `answers` (`id`, `user_id`, `question_id`, `description`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 1, '<p>16 core up</p>\r\n', '2017-04-24', '0000-00-00', '0000-00-00'),
(3, 2, 4, '<p>tarporeo tui jor korate answer korlam thak eta korar dorkar nai</p>\r\n\r\n<p>&nbsp;</p>\r\n', '2017-04-24', '2017-04-24', '0000-00-00'),
(4, 1, 1, '<p>your idea is good&nbsp;</p>\r\n', '2017-04-25', '0000-00-00', '0000-00-00'),
(5, 1, 3, '<p>question id 3 te ekta question kori tahole</p>\r\n', '2017-04-25', '0000-00-00', '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE `questions` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `title` varchar(80) NOT NULL,
  `description` varchar(200) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`id`, `user_id`, `title`, `description`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'How many People Lives In Bangldesh?', '<p>Hello amarproshno,</p>\r\n\r\n<p>I am talking about the people of Bangldesh please answer me How many People live in Bangladesh?</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Bangladesh is a very small country there prop', '2017-04-24', '2017-04-25', '0000-00-00'),
(3, 1, 'Is there an app that you hate but use anyways?', '<p>Is there an app that Is there an app that you hate but use anyways?yways?hat you hate bIs there an Is there anIs there an app that you hate but use anyways? that you hate but use anyways?hate but u', '2017-04-24', '0000-00-00', '0000-00-00'),
(4, 1, 'Could you survive in the wilderness for a month?', '<p><img alt=\"\" src=\"http://combiboilersleeds.com/images/way/way-0.jpg\" style=\"border-style:solid; border-width:2px; height:125px; margin-left:50px; margin-right:50px; width:200px\" /></p>\r\n\r\n<p>Is ther', '2017-04-24', '0000-00-00', '0000-00-00'),
(5, 2, 'How to use twitter? and how can i sign up  on twitter', '<p>twitter is the second largest social media platform in all over the world</p>\r\n', '2017-04-24', '0000-00-00', '0000-00-00'),
(6, 3, 'What is php? how it works? what is the main defination of it?', '<p>i know about html that is a just markup language. but php and javascript and many other proramming language is server side scripting lnguage now my question about php what is this and how it works?', '2017-04-24', '0000-00-00', '0000-00-00'),
(7, 1, 'What is BITM? How many course BITM  Provide in free of coast?', '<p>I know something about Bitm. But I have a question about Bitm How BITM works ?</p>\r\n\r\n<p>what is there resource. How many trainer they have ?</p>\r\n', '2017-04-25', '0000-00-00', '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `is_admin` varchar(2) NOT NULL DEFAULT '0',
  `first_name` varchar(10) NOT NULL,
  `last_name` varchar(10) NOT NULL,
  `middle_name` varchar(10) NOT NULL,
  `gender` enum('M','F') NOT NULL,
  `hobby` varchar(15) NOT NULL,
  `interest` varchar(15) NOT NULL,
  `email` varchar(60) NOT NULL,
  `password` char(32) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date NOT NULL,
  `photo` varchar(120) NOT NULL DEFAULT 'assets/images/user.jpg',
  `date_of_birth` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `is_admin`, `first_name`, `last_name`, `middle_name`, `gender`, `hobby`, `interest`, `email`, `password`, `created_at`, `updated_at`, `deleted_at`, `photo`, `date_of_birth`) VALUES
(1, '1', 'Anwarul', 'Bablu', 'Islam', 'M', '', '', 'aib.tmkm@gmail.com', '651830', '2017-04-24', '0000-00-00', '0000-00-00', 'assets/images/user.jpg', '0000-00-00'),
(2, '0', 'Abdullah', 'Mahmud', 'Al', 'M', '', '', 'mahmud@mahmud.com', '123456', '2017-04-24', '0000-00-00', '0000-00-00', 'assets/images/user.jpg', '0000-00-00'),
(3, '0', 'Abdullah', 'Ma\'ruf', 'Al', '', '', '', 'maruf@maruf.com', '651830', '2017-04-24', '0000-00-00', '0000-00-00', 'assets/images/user.jpg', '0000-00-00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `answers`
--
ALTER TABLE `answers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `answers`
--
ALTER TABLE `answers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `questions`
--
ALTER TABLE `questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
